-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 08, 2019 at 09:40 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `electronic-devices`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `Username` varchar(20) NOT NULL,
  `Password` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`Username`, `Password`) VALUES
('admin', 'pass123');

-- --------------------------------------------------------

--
-- Table structure for table `commerce`
--

CREATE TABLE `commerce` (
  `RName` varchar(20) NOT NULL,
  `CName` varchar(20) NOT NULL,
  `DName` varchar(20) NOT NULL,
  `Price` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `commerce`
--

INSERT INTO `commerce` (`RName`, `CName`, `DName`, `Price`) VALUES
('Amazon', 'Apple', 'iPad Pro 11', '1349.99'),
('Amazon', 'Nokia', 'Nokia 9 pureview', '449.99'),
('Amazon', 'OnePlus', 'OnePlus 7 Pro', '890.00'),
('Best Buy', 'Apple', 'iPhone 11 Pro ', '1349.99'),
('Best Buy', 'Apple', 'iPhone 11 Pro Max', '1449.99'),
('Best Buy', 'Microsoft', 'Surface Laptop 3', '2099.00'),
('Best Buy', 'Samsung', 'Galaxy Note 10+', '1449.99'),
('Sam\'s Club', 'Dell', 'Dell XPS 15', '1799.99'),
('Walmart', 'Google', 'Pixel Slate', '1599.00');

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `Name` varchar(20) NOT NULL,
  `Website` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`Name`, `Website`) VALUES
('Apple', 'https://www.apple.com/'),
('Dell', 'https://www.dell.com/en-us'),
('Google', 'https://store.google.com/us/'),
('Microsoft', 'https://www.microsoft.com/en-us/'),
('Nokia', 'https://www.nokia.com/nokia-corporation-1/'),
('OnePlus', 'https://www.oneplus.com/'),
('Samsung', 'https://www.samsung.com/us/');

-- --------------------------------------------------------

--
-- Table structure for table `device`
--

CREATE TABLE `device` (
  `Name` varchar(50) NOT NULL,
  `Year` year(4) NOT NULL,
  `RAM` int(20) DEFAULT NULL COMMENT 'in GB',
  `CPU` varchar(50) DEFAULT NULL,
  `GPU` varchar(50) DEFAULT NULL,
  `OS` varchar(50) DEFAULT NULL,
  `IO` varchar(50) DEFAULT NULL,
  `Battery` int(20) DEFAULT NULL COMMENT 'in mAh',
  `Storage` int(20) DEFAULT NULL COMMENT 'in GB',
  `Camera` varchar(100) DEFAULT NULL,
  `Display` varchar(50) DEFAULT NULL,
  `Device_Type` enum('phone','tablet','laptop','') NOT NULL,
  `Carrier` varchar(200) DEFAULT NULL,
  `Keyboard` varchar(50) DEFAULT NULL,
  `Pen_Compatibility` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `device`
--

INSERT INTO `device` (`Name`, `Year`, `RAM`, `CPU`, `GPU`, `OS`, `IO`, `Battery`, `Storage`, `Camera`, `Display`, `Device_Type`, `Carrier`, `Keyboard`, `Pen_Compatibility`) VALUES
('Dell XPS 15', 2019, 16, 'Intel Core i7-8750H ', NULL, 'Windows  10', NULL, NULL, 512, '720 HD cam', '15\'', 'laptop', NULL, 'Yes', NULL),
('Galaxy Note 10+', 2019, 12, 'Octa-core', 'Adreno 640', 'Android 9.0 (Pie)', 'USB-C', 4300, 512, '12 MP, f/1.5-2.4, 27mm (wide), 1/2.55\", 1.4µm, Dual Pixel PDAF, OIS\r\n12 MP, f/2.', '6.8 in 1440 x 3040 AMOLED', 'phone', 'AT&T, Sprint, T-Mobile, Verizon', NULL, NULL),
('iPad Pro 11', 2019, 6, 'Octa-core', 'Apple GPU', 'iOS 12', 'USB-C', 7812, 1024, '12 MP, f/1.8, 29mm (standard), 1/3\", 1.22µm, PDAF', '1668 x 2388 pixels', 'tablet', NULL, NULL, 1),
('iPhone 11 Pro ', 2019, 4, 'Hexa-core ', 'Apple GPU', 'iOS 13', 'lightning', 3969, 512, '12 MP, f/1.8, 26mm (wide), 1/2.55\", 1.4µm, dual pixel PDAF, OIS\r\n12 MP, f/2.0, 5', '1242 x 2688 pixels, ', 'phone', 'AT&T, Sprint, T-Mobile, Verizon', NULL, NULL),
('iPhone 11 Pro Max', 2019, 4, 'A13 Bionic chip', NULL, 'iOS 13', 'lightning', 3969, 512, 'Triple 12MP Ultra Wide, Wide, and Telephoto', '6.5in 2688 x 1242 Super Retina XDR', 'phone', 'AT&T, Sprint, T-Mobile, Verizon', NULL, NULL),
('Nokia 9 pureview', 2019, 6, 'Octa-core', 'Adreno 630', 'Android 9.0 (Pie)', 'Type-C', 3320, 128, '5x 12 MP, f/1.8, 28mm (wide), 1/2.9\", 1.25µm\r\n', '1440 x 2880', 'phone', NULL, 'NA', NULL),
('OnePlus 7 Pro', 2019, 8, 'Qualcomm Snapdragon Octa-core', 'Adreno 640', 'Android 10.0', 'USB-C', 4085, 256, '48 MP, f/1.6, (wide), 1/2\", 0.8µm, PDAF, Laser AF, OIS\r\n8 MP, f/2.4, 78mm (telephoto)', '6.67in 1440 x 3120 QHD+ OLED', 'phone', 'AT&T, Sprint, T-Mobile, Verizon', NULL, NULL),
('Pixel Slate', 2019, 16, 'Intel Core i7 Proces', NULL, 'Chrome OS', 'USB-C', 48, 256, '8-megapixel rear camera, ƒ/1.8 aperture, 1.12um pixel size, Auto Focus, 1080p 30', '12.3? Molecular Disp', 'tablet', NULL, 'NA', NULL),
('Surface Laptop 3', 2019, 16, 'AMD Ryzen 7 3780U', 'AMD Radeon RX Vega 11', 'Windows 10 ', 'USB-C', NULL, 512, '720p HD webcam', '15in 2,496 x 1,664 Touchscreen LCD', 'laptop', NULL, 'Backlit', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `parts`
--

CREATE TABLE `parts` (
  `Name` varchar(20) NOT NULL,
  `Manufacturer` varchar(20) DEFAULT NULL,
  `Type` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `parts`
--

INSERT INTO `parts` (`Name`, `Manufacturer`, `Type`) VALUES
('48 MP, f/1.8', 'Zeiss', 'Camera'),
('5.8in OLED', 'LG', 'Display'),
('A13 Bionic', 'Apple', 'CPU'),
('Android 10.0', 'Google', 'OS'),
('iOS 13', 'Apple', 'OS');

-- --------------------------------------------------------

--
-- Table structure for table `parts_bought`
--

CREATE TABLE `parts_bought` (
  `CName` varchar(20) NOT NULL,
  `PName` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `parts_bought`
--

INSERT INTO `parts_bought` (`CName`, `PName`) VALUES
('Apple', '5.8in OLED');

-- --------------------------------------------------------

--
-- Table structure for table `retailer`
--

CREATE TABLE `retailer` (
  `Name` varchar(20) NOT NULL,
  `Website` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `retailer`
--

INSERT INTO `retailer` (`Name`, `Website`) VALUES
('Amazon', 'https://www.amazon.com'),
('Best Buy', 'https://www.bestbuy.com/'),
('Newegg', 'https://www.newegg.com/'),
('Sam\'s Club', 'https://www.samsclub.com/'),
('Walmart', 'https://www.walmart.com/');

-- --------------------------------------------------------

--
-- Table structure for table `retailer_location`
--

CREATE TABLE `retailer_location` (
  `RName` varchar(20) NOT NULL,
  `Location` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `retailer_location`
--

INSERT INTO `retailer_location` (`RName`, `Location`) VALUES
('Amazon', 'Online'),
('Best Buy', 'Fenton'),
('Best Buy', 'Jefferson City'),
('Best Buy', 'Online'),
('Best Buy', 'South County');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`Username`);

--
-- Indexes for table `commerce`
--
ALTER TABLE `commerce`
  ADD PRIMARY KEY (`RName`,`CName`,`DName`),
  ADD KEY `DName` (`DName`),
  ADD KEY `CName` (`CName`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`Name`);

--
-- Indexes for table `device`
--
ALTER TABLE `device`
  ADD PRIMARY KEY (`Name`);

--
-- Indexes for table `parts`
--
ALTER TABLE `parts`
  ADD PRIMARY KEY (`Name`);

--
-- Indexes for table `parts_bought`
--
ALTER TABLE `parts_bought`
  ADD PRIMARY KEY (`CName`,`PName`),
  ADD KEY `PName` (`PName`);

--
-- Indexes for table `retailer`
--
ALTER TABLE `retailer`
  ADD PRIMARY KEY (`Name`);

--
-- Indexes for table `retailer_location`
--
ALTER TABLE `retailer_location`
  ADD PRIMARY KEY (`RName`,`Location`) USING BTREE;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `commerce`
--
ALTER TABLE `commerce`
  ADD CONSTRAINT `commerce_ibfk_1` FOREIGN KEY (`DName`) REFERENCES `device` (`Name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `commerce_ibfk_2` FOREIGN KEY (`RName`) REFERENCES `retailer` (`Name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `commerce_ibfk_3` FOREIGN KEY (`CName`) REFERENCES `company` (`Name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `parts_bought`
--
ALTER TABLE `parts_bought`
  ADD CONSTRAINT `parts_bought_ibfk_1` FOREIGN KEY (`CName`) REFERENCES `company` (`Name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `parts_bought_ibfk_2` FOREIGN KEY (`PName`) REFERENCES `parts` (`Name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `retailer_location`
--
ALTER TABLE `retailer_location`
  ADD CONSTRAINT `retailer_location_ibfk_1` FOREIGN KEY (`RName`) REFERENCES `retailer` (`Name`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
