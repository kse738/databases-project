<?php
include 'classes/dbh.class.php';
include 'classes/device.class.php';
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="description" content="Electronics Database">
    <title>
        Electronics Database
    </title>
    <link href="./../css/bruh.css" rel="stylesheet" type="text/css">
    
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script type="text/javascript" src="./../js/jquery-ui.js"></script>
    <script type="text/javascript" src="./../js/fadein.js"></script>

</head>

<body>
    <div>
        <br/>
        <!----------------------------NAMES AND HEADER STYLING-------------------------->
        <div class="names">
            <div class="sub">
                Kevin Ehlen
            </div>
            <div class="sub">
                John Shenouda
            </div>
            <div class="sub">
                <a href="https://www.youtube.com/watch?v=FuraQCCsKgE" target="_self" style="text-decoration: none; color: rgb(175, 175, 175);">Katherine Miller</a>
            </div>
        </div><br/>
        <hr class="topline"/><br/>
        <h1>
           <a href="https://www.youtube.com/watch?v=dQw4w9WgXcQ" target="_self" style="text-decoration: none; color: rgb(20, 20, 20);"><strong>ELECTRONICS DATABASE</strong></a> 
        </h1>
    </div>
    <div class="subtitle">
        <p>
            <em>in case you didn't know how to google things</em>
        </p>
    </div>
    <br/><hr class="bottomline">

    <!----------------------------NAIGATION BAR-------------------------->
    <?php
        require "mainheader.php";

        //if (isset($_SESSION['userID'])) {
      //      echo '<p class="login-status">You are logged in as admin!</p>';
       // }
      //  else {
      //      echo '<p class="login-status">You are logged out!</p>';
      //  }

    ?>      
     

    <!----------------------------TABLE OF IMAGES-------------------------->
    <div class="row">
        <div class="col">
            <a href="https://www.oneplus.com/oneplus-7pro?from=op7pro_header" target="_blank"><img class="responsive" src="https://s4.nhattao.com/data/attachment-files/2019/06/12645102_oneplus_7_pro_128gb_dual_sim_almond.jpg" 
                style="margin-left: 70px; margin-top: 10px;"></a> 
            <section>
                <a href="https://www.microsoft.com/en-us/p/surface-go/8v9dp4lnknsz?activetab=pivot:techspecstab" target="_blank"><img class="responsive" src="https://www.bhphotovideo.com/images/images2500x2500/microsoft_kaz_00001_surface_go_10_tablet_1543524653000_1445353.jpg" 
                    style="margin-left: 50px;"></a>
                <a href="https://www.samsung.com/us/mobile/galaxy-note10/buy/" target="_blank"><img class="responsive" src="https://images-na.ssl-images-amazon.com/images/I/61LGgNjwg1L._SL1500_.jpg" style="margin-left: 110px; margin-top: 80px;"></a>
            </section>
        </div>
        <div class="col">
            <a href="https://www.apple.com/shop/buy-mac/macbook-pro/13-inch-silver-2.4ghz-quad-core-processor-with-turbo-boost-up-to-4.1ghz-512gb#" target="_blank"><img class="responsive" src="https://gsmart.pk/wp-content/uploads/2019/06/mbp15touch-silver-select-201807po.jpg" 
                style="margin-top: 50px; margin-left: 60px;"></a>
            <section>
                <a href="https://www.apple.com/shop/buy-iphone/iphone-11" target="_blank"><img class="responsive" src="https://image.migros.ch/product-zoom/366c9afab9dbb3b29aafadb6bdf4fbe0668fc56c.jpg" style="margin-left: 70px; margin-top: 115px;"/></a>
                <a href="https://www.microsoft.com/en-us/store/config/Surface-Laptop-2/8XQJKK3DD91B?crosssellid=surfacefamily4a&selectedColor=000000&preview=&previewModes=" target="_blank"><img class="responsive" 
                    src="https://pisces.bbystatic.com/image2/BestBuy_US/images/products/6134/6134347_sd.jpg" style="margin-left: 30px; margin-top: 170px;"></a>
            </section>
        </div>
    </div>

    <?php
        require "footer.php";
    ?>

</body>
</html>