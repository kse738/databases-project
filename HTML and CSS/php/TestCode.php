<?php

//include 'includes/class-autoload.inc.php';
include 'classes/dbh.class.php';
include 'classes/part.class.php';
include 'classes/retailer.class.php';
include 'classes/device.class.php';

// ------ General SELECT query without needing to connect to DB when called ------
$dbh = new Dbh();
$company = "Apple";             // (attr, table(S), condition)
$rowsArr = $dbh->getDefinedSelQuery("*", "device, commerce", "Dname = Name and CName = '$company'");
// ------ ------
?>

<! DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <!-- To insert parts -->
    <form action="includes/part.inc.php" method="post">
      <p>Insert part</p>
      <input type="text" name="pName" placeholder="Part name">
      <input type="text" name="manuf" placeholder="Manufacturer">
      <select name="pType">
        <option value="Display">Display</option>
        <option value="RAM">RAM</option>
        <option value="CPU">CPU</option>
        <option value="GPU">GPU</option>
        <option value="OS">OS</option>
        <option value="IO">IO</option>
        <option value="Battery">Battery</option>
        <option value="Storage">Storage</option>
        <option value="Camera">Camera</option>
      </select>
      <button type="submit" name="submit">Insert</button>
    </form>
    <!-- To insert retailer -->
    <form action="includes/retailer.inc.php" method="post">
      <p>Insert retailer</p>
      <input type="text" name="rName" placeholder="Retailer name">
      <input type="text" name="web" placeholder="Website">
      <button type="submit" name="submitR">Insert</button>
    </form>

    <!-- To insert device -->
    <form action="includes/device.inc.php" method="post">
      <p>Insert Device</p>
      <input type="text" name="dName" placeholder="Device name">
      <input type="text" name="year" placeholder="Year">
      <!-- Specs need to be selected from existing parts, otherwise insert new part -->
      <input type="text" name="ram" placeholder="RAM">
      <input type="text" name="cpu" placeholder="CPU">
      <input type="text" name="gpu" placeholder="GPU">
      <input type="text" name="os" placeholder="OS">
      <input type="text" name="io" placeholder="IO">
      <input type="text" name="battery" placeholder="Battery">
      <input type="text" name="storage" placeholder="Storage">
      <input type="text" name="camera" placeholder="Camera">
      <input type="text" name="display" placeholder="Display">
      <select name="dType">
        <option value="phone">Phone</option>
        <option value="laptop">Laptop</option>
        <option value="tablet">Tablet</option>
      </select>
      <!-- Only appears if phone selected -->
      <input type="text" name="carrier" placeholder="Carrier">
      <!-- Only appears if laptop selected -->
      <input type="text" name="keyboard" placeholder="Keyboard">
      <!-- Only appears if tablet selected -->
      <!-- Needs to be 1 or 0 for boolean value -->
      <input type="text" name="penComp" placeholder="Pen Compatability">

      <input type="submit" name="submitD" value="Insert"/>
    </form>

    <!-- To delete device -->
    <form action="includes/device.inc.php" method="post">
      <p>Delete Device</p>
      <input type="text" name="dNameDelete" placeholder="Device Name">
      <input type="submit" name="submitDdelete" value="Delete"/>
    </form>

  <br/><br/><br/><br/><br/>
  <h2 style="font-size: 20pt; font-style: italic; color: rgb(61, 61, 61);">Devices by <?php echo $company ?></h2>

  <table>
    <thead>
      <tr>
        <th>Device Name</th>
        <th>Year</th>
        <th>RAM (in GB)</th>
        <th>CPU</th>
        <th>OS</th>
        <th>IO</th>
        <th>Battery (in mAh)</th>
        <th>Storage (in GB)</th>
        <th>Camera</th>
        <th>Display</th>
        <th>Carrier</th>
        <th>Price</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($rowsArr as $row): ?>
        <tr>
          <td><?php echo htmlspecialchars($row['Name']) ?></td>
          <td><?php echo htmlspecialchars($row['Year']) ?></td>
          <td><?php echo htmlspecialchars($row['RAM']) ?></td>
          <td><?php echo htmlspecialchars($row['CPU']) ?></td>
          <td><?php echo htmlspecialchars($row['OS']) ?></td>
          <td><?php echo htmlspecialchars($row['IO']) ?></td>
          <td><?php echo htmlspecialchars($row['Battery']) ?></td>
          <td><?php echo htmlspecialchars($row['Storage']) ?></td>
          <td><?php echo htmlspecialchars($row['Camera']) ?></td>
          <td><?php echo htmlspecialchars($row['Display']) ?></td>
          <td><?php echo htmlspecialchars($row['Carrier']) ?></td>
          <td><?php echo htmlspecialchars($row['Price']) ?></td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>

  <!-- For admin login -->
  <form action="includes/admin.inc.php" method="post">
    <p>Admin Login</p>
    <input type="text" name="uName" placeholder="Username">
    <input type="text" name="pwd" placeholder="Password">
    <button type="submit" name="login">Login</button>
  </form>

  </body> 
</html>
