<?php
include 'classes/dbh.class.php';
include 'classes/admin.class.php';

$admin = new Admin(NULL,NULL);

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="description" content="Apple Brand Page">
    <title>
        Login
    </title>
    <link href="../css/apple.css" rel="stylesheet" type="text/css">
    <link href="../css/input.css" rel="stylesheet" type="text/css">
</head>

<body>
  <!----------------------------PAGE HEADER AND NAIGATION BAR-------------------------->
  <?php
    require "shopPageHeader.php";
  ?>


  <div class="in">
    <?php
      if (isset($_SESSION['userID']) == NULL) {   //If not logged in, show login box and "Admin Login" label at top
        echo
          '<br/><br/><br/>
            <h2 style="font-size: 20pt; font-style: italic; color: rgb(61, 61, 61);">Admin Login</h2>
          <br/><br/><br/><br/><br/>

          <form class="form-inline" action="includes/login.inc.php" method="post">
            <input type="text" style="margin-left:40%;" name="userName" placeholder="Username">
            <input type="password" name="pwd" placeholder="Password">
            <button type="submit" style="cursor: pointer; margin-left: 5px; background-color: #ddd; border: 1px solid rgb(190, 190, 190); padding: 3px; width: 3%;" name="login-submit">Login</button>
          </form>';
      }
      else {         //Else, user is logged in so show message
        echo 
          '<br/><br/><br/>
            <h2 style="font-size: 20pt; font-style: italic; color: rgb(61, 61, 61);">You are logged in as admin!</h2>
          <br/><br/><br/><br/><br/>';
      }
      
    ?>
    

  
<?php
  // Error/success messages
  if (isset($_GET['error'])) {
   // if ($_GET['error'] == "emptyfields") {
      //echo '<br/><br/><br/> <h2 style="font-size: 20pt; font-style: italic; color: rgb(61, 61, 61);">Fill in all fields!</h2>';
    //}
    if ($_GET['error'] == "wrongpwd") {
      echo '<br/><br/><br/> <h2 style="font-size: 20pt; font-style: italic; color: rgb(61, 61, 61);">Incorrect Password!</h2>';
    }
    else if ($_GET['error'] == "nouser") {
      echo '<br/><br/><br/> <h2 style="font-size: 20pt; font-style: italic; color: rgb(61, 61, 61);">User does not exist!</h2>';
    }
  }


  //else if (isset($_GET['success'])) {
    //if ($_GET['login'] == "success") {
     // echo '<br/><br/><br/> <p style="margin-left:45%;" class="loginsuccess">Login was successful!</p>';
    //}
 // }


?>
</div>
</body>
</html>