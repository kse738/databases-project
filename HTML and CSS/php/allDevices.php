<?php
include 'classes/dbh.class.php';
include 'classes/device.class.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="description" content="Apple Brand Page">
    <title>
      Devices
    </title>
    <link href="/css/apple.css" rel="stylesheet" type="text/css">
    <link href="/css/input.css" rel="stylesheet" type="text/css">
    <link href="/css/deviceInput.css" rel="stylesheet" type="text/css">
</head>


<body>
  <!----------------------------PAGE HEADER AND NAIGATION BAR-------------------------->
  <?php
    require "shopPageHeader.php";
  ?>
  <br/><br/><br/>
  <h2 style="font-size: 20pt; font-style: italic; color: rgb(61, 61, 61);">Devices</h2>

  <!----------------------------Search Bar-------------------------->
  <script>
    <?php
      require "../js/searchBar.js";
    ?>
  </script>
 
  <form class="form-inline">
    <input type="text" style="margin-left: 78%; width: 10%;" id="dInput" onkeyup="searchTable('devices','dInput')" placeholder="Search Device Name">
  </form>
  <br/><br/><br/><br/><br/>


  <!----------------------------TABLE OF DEVICES-------------------------->
  <table class="smallerTable" id="devices">
      <?php
        require "filters/filter.php";
      ?>
    <tbody>
      <?php 
      foreach ($rowsArr as $row) {
        echo "<tr>
                <td>".$row['Name']."</td>
                <td>".$row['Year']."</td>
                <td>".$row['CName']."</td>
                <td>".$row['RAM']."GB</td>
                <td>".$row['CPU']."</td>
                <td>".$row['OS']."</td>
                <td>".$row['IO']."</td>
                <td>".$row['Battery']."mAh</td>
                <td>".$row['Storage']."GB</td>
                <td>".$row['Camera']."</td>
                <td>".$row['Display']."</td>
                <td>".$row['Carrier']."</td>
              </tr>";
      }
      ?>
    </tbody>
  </table>

<br/><br/>
<?php
  require "footer.php";
?>
</body>
</html>