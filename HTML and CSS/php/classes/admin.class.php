<?php

class Admin extends Dbh {
  private $adminName;
  private $password;
  private $isLoggedin;


  public function __construct($name, $pass) {
    $this->adminName = $name;
    $this->password = $pass;
    $this->isLoggedin = false;
  }


  // --------- Insert Update Delete Methods ---------

  public function updateAdmin() {
    $sql = "UPDATE admins SET Username =?, Password =?";
    $stmt = $this->connect()->prepare($sql);
    $stmt->execute([$this->adminName,  $this->password]);
    //header("location:http://localhost/php/PartList.php"); // Different page for admin login
  }

  public function adminLogin() {
    $sql = "SELECT * FROM admins WHERE Username=?";
    $stmt = $this->connect()->prepare($sql);
    $stmt->execute([$this->adminName]);
    
    if ($row = $stmt->fetch()) {
      // If hashed:
      // $pwdCheck = password_verify($this->password, $row['Password']);
      $pwdCheck = ($this->password == $row['Password'] ? true : false);
      if ($pwdCheck == false) {
        header("Location: ../adminLogin.php?error=wrongpwd");
      exit();
      }
      else if ($pwdCheck == true) {
        // Now Login the admin
        session_start();
        $_SESSION['userID'] = $row['Username'];
        header("Location: ../adminLogin.php?login=success");
        exit();
      }
    }
    else {
      header("Location: ../adminLogin.php?error=nouser");
      exit();
    }
  }

  // public function login() {
  //   if ($this->adminExists()) {
  //     return true;
  //   }
  //   return false;
  // }

  // public function checkLoggedin() {
  //   return $this->isLoggedin;
  // }
}