<?php

class Dbh {
  private $host = "localhost";
  private $user = "root";
  private $pwd = ""; 
  private $dbName = "electronic-devices";

  protected function connect() {
    try {
      $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbName;
      $pdo = new PDO($dsn, $this->user, $this->pwd);
      $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
      return $pdo;
    } catch (PDOException $e) {
      echo "Connection failed: ".$e->getMessage();
    }
  }

  // ------ General SELECT query without needing to connect to DB when called ------
  public function getDefinedSelQuery($attr, $table, $cond = "1") {
    $rowsArr = array();
    $sql = "SELECT $attr FROM $table WHERE $cond";
    $stmt = self::connect()->query($sql);
    while ($row = $stmt->fetch()) {
      array_push($rowsArr, $row);
    }
    return $rowsArr;
  }

  public function getFilter($attr, $table, $order, $cond = "1") {
    $rowsArr = array();
    $sql = "SELECT $attr FROM $table WHERE $cond ORDER BY $order ASC";
    $stmt = self::connect()->query($sql);
    while ($row = $stmt->fetch()) {
      array_push($rowsArr, $row);
    }
    return $rowsArr;
  }

  public function filterDesc($attr, $table, $order, $cond = "1") {
    $rowsArr = array();
    $sql = "SELECT $attr FROM $table WHERE $cond ORDER BY $order DESC";
    $stmt = self::connect()->query($sql);
    while ($row = $stmt->fetch()) {
      array_push($rowsArr, $row);
    }
    return $rowsArr;
  }

  public function getSingle($attr, $table, $cond = "1") {
    $rowsArr = array();
    $sql = "SELECT $attr FROM $table WHERE $cond";
    $stmt = self::connect()->query($sql);

    $row = $stmt->fetch();

    return $row;
  }
}