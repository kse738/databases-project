<?php

class Device extends Dbh {
  private $attributes = array(
    "dName" => "",
    "year" => "",
    "ram" => "",
    "cpu" => "",
    "gpu" => "",
    "os" => "",
    "io" => "",
    "battery" => "",
    "storage" => "",
    "camera" => "",
    "display" => "",
    "deviceType" => "",
    "carrier" => "",
    "keyboard" => "",
    "penComp" => ""
  );

  public function __construct($name, $year, $type) {
    foreach ($this->attributes as $i => $value) {
      if ($i == "dName") {
        $this->attributes[$i] = $name;
      }
      else if ($i == "year") {
        $this->attributes[$i] = $year;
      }
      else if ($i == "deviceType") {
        $this->attributes[$i] = $type;
      }
      else {
        $this->attributes[$i] = NULL;
      }
    }
  }


  // --------- Insert Update Delete Methods ---------

  public function insDevice() {
    $sql = "INSERT INTO device (Name, Year, RAM, CPU, GPU, OS, IO, 
                                Battery, Storage, Camera, Display, 
                                Device_Type, Carrier, Keyboard, Pen_Compatibility) 
                                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    $stmt = $this->connect()->prepare($sql);
    $stmt->execute([$this->dName = $this->attributes["dName"], 
                    $this->attributes["year"], 
                    $this->attributes["ram"], 
                    $this->attributes["cpu"], 
                    $this->attributes["gpu"], 
                    $this->attributes["os"], 
                    $this->attributes["io"], 
                    $this->attributes["battery"], 
                    $this->attributes["storage"], 
                    $this->attributes["camera"], 
                    $this->attributes["display"], 
                    $this->attributes["deviceType"],  
                    $this->attributes["carrier"], 
                    $this->attributes["keyboard"], 
                    $this->attributes["penComp"]]);
    header("location:/php/inserting/insertDevices.php");
  }

  public function updateDevice() {
    $sql = "UPDATE device SET Year =?, RAM =?, CPU =?, GPU =?, OS =?, IO =?, 
                              Battery =?, Storage =?, Camera =?, Display =?, 
                              Device_Type =?, Carrier =?, Keyboard =?, Pen_Compatibility =? 
                              WHERE Name =?";
    $stmt = $this->connect()->prepare($sql);
    $stmt->execute([$this->attributes["year"], 
                    $this->attributes["ram"], 
                    $this->attributes["cpu"], 
                    $this->attributes["gpu"], 
                    $this->attributes["os"], 
                    $this->attributes["io"], 
                    $this->attributes["battery"], 
                    $this->attributes["storage"], 
                    $this->attributes["camera"], 
                    $this->attributes["display"], 
                    $this->attributes["deviceType"], 
                    $this->attributes["carrier"], 
                    $this->attributes["keyboard"], 
                    $this->attributes["penComp"],
                    $this->attributes["dName"]]);
    header("location:/php/inserting/insertDevices.php");
  }

  public function deleteDevice($dName) {
    $sql = "DELETE FROM device WHERE Name=?";
    $stmt = $this->connect()->prepare($sql);
    $stmt->execute([$dName]);
    header("location:/php/inserting/insertDevices.php");
  }


  // --------- Set Methods --------- 

  public function setAttribute($key, $value) {
    $this->attributes[$key] = $value;
  }


  // --------- Other Methods ---------

  public function getAllDevices() {
    $rowsArr = array();
    $sql = "SELECT * FROM device";
    $stmt = $this->connect()->query($sql);
    while ($row = $stmt->fetch()) {
      array_push($rowsArr, $row);
    }
    return $rowsArr;
  }

  public function getDevice($dName) {  
    $rowsArr = array();
    $sql = "SELECT * FROM device ORDER BY $dName ASC";
    $stmt = $this->connect()->query($sql);
    while ($row = $stmt->fetch()) {
      array_push($rowsArr, $row);
    }

    return $rowsArr;
  }



  public function keyExists() {
    $sql = "SELECT * FROM device WHERE Name =?";
    $stmt = $this->connect()->prepare($sql);
    $stmt->execute([$this->attributes["dName"]]);

    if ($stmt->rowCount()) {
      return true;
    }
    else {
      return false;
    }
  } 

}