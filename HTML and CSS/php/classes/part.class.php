<?php
//include_once 'dbh.class.php';

class Part extends Dbh{
  private $pName;
  private $manuf;
  private $pType;

  public function __construct($name, $man, $type) {
    $this->pName = $name;
    $this->manuf = $man;
    $this->pType = $type;
  }


  // --------- Insert Update Delete Methods ---------

  public function insPart() {
    $sql = "INSERT INTO parts(Name, Manufacturer, Type) VALUES (?, ?, ?)";
    $stmt = $this->connect()->prepare($sql);
    $stmt->execute([$this->pName, $this->manuf, $this->pType]);
    header("location:/php/inserting/PartList.php");
  }

  public function updatePart() {
    $sql = "UPDATE parts SET Manufacturer =?, Type =? WHERE Name =?";
    $stmt = $this->connect()->prepare($sql);
    $stmt->execute([$this->manuf,  $this->pType, $this->pName]);
    header("location:/php/inserting/PartList.php");
  }


  // --------- Set Methods --------- 

  public function setName($name) {
    $this->pName = $name;
  }

  public function setManuf($man) {
    $this->$manuf = $man;
  }

  public function setpType($type) {
    $this->$pType = $type;
  }


  // --------- Get Methods --------- 

  public function getPName() {
    return $this->pName;
  }

  public function getmanuf() {
    return $this->manuf;
  }

  public function getPType() {
    return $this->pType;
  }


  // --------- Other Methods ---------

  public function getAllParts() {
    $rowsArr = array();
    $sql = "SELECT * FROM parts";
    $stmt = $this->connect()->query($sql);
    while ($row = $stmt->fetch()) {
      array_push($rowsArr, $row);
    }
    return $rowsArr;
  }

  public function keyExists() {
    $sql = "SELECT * FROM parts WHERE Name =?";
    $stmt = $this->connect()->prepare($sql);
    $stmt->execute([$this->pName]);

    if ($stmt->rowCount()) {
      return true;
    }
    else {
      return false;
    }
  }

}