<?php

class Retailer extends Dbh {
  private $rName;
  private $website;

  public function __construct($name, $web) {
    $this->rName = $name;
    $this->website = $web;
  }

  
  // --------- Insert Update Delete Methods ---------

  public function insRetailer() {
    $sql = "INSERT INTO retailer(Name, Website) VALUES (?, ?)";
    $stmt = $this->connect()->prepare($sql);
    $stmt->execute([$this->rName, $this->website]);
    header("location:/php/inserting/PartList.php");
  }

  public function updateRetailer() {
    $sql = "UPDATE retailer SET Website =? WHERE Name =?";
    $stmt = $this->connect()->prepare($sql);
    $stmt->execute([$this->website, $this->rName]);
    header("location:/php/inserting/PartList.php");
  }


  // --------- Set Methods --------- 

  public function setName($name) {
    $this->rName = $name;
  }

  public function setWeb($web) {
    $this->website = $web;
  }


  // --------- Other Methods ---------

  public function getAllRetailers() {
    $rowsArr = array();
    $sql = "SELECT * FROM retailer";
    $stmt = $this->connect()->query($sql);
    while ($row = $stmt->fetch()) {
      array_push($rowsArr, $row);
    }
    return $rowsArr;
  }

  public function keyExists() {
    $sql = "SELECT * FROM retailer WHERE Name =?";
    $stmt = $this->connect()->prepare($sql);
    $stmt->execute([$this->rName]);

    if ($stmt->rowCount()) {
      return true;
    }
    else {
      return false;
    }
  }
}