<?php

if(isset($_POST['fDevice'])) {          
  $dbh = new Dbh();
  $rowsArr = $dbh->getFilter("*", "device, commerce", "Name", "Dname = Name");
}
else if (isset($_POST['fYear'])) {
  $dbh = new Dbh();
  $rowsArr = $dbh->filterDesc("*", "device, commerce", "Year", "Dname = Name");
}
else if (isset($_POST['fCompany'])) {
  $dbh = new Dbh();
  $rowsArr = $dbh->getFilter("*", "device, commerce", "Company", "Dname = Name");
}
else if (isset($_POST['fRAM'])) {
  $dbh = new Dbh();
  $rowsArr = $dbh->filterDesc("*", "device, commerce", "RAM", "Dname = Name");
}
else if (isset($_POST['fCPU'])) {
  $dbh = new Dbh();
  $rowsArr = $dbh->getFilter("*", "device, commerce", "CPU", "Dname = Name");
}
else if (isset($_POST['fOS'])) {
  $dbh = new Dbh();
  $rowsArr = $dbh->getFilter("*", "device, commerce", "OS", "Dname = Name");
}
else if (isset($_POST['fIO'])) {
  $dbh = new Dbh();
  $rowsArr = $dbh->getFilter("*", "device, commerce", "IO", "Dname = Name");
}
else if (isset($_POST['fBattery'])) {
  $dbh = new Dbh();
  $rowsArr = $dbh->filterDesc("*", "device, commerce", "Battery", "Dname = Name");
}
else if (isset($_POST['fStorage'])) {
  $dbh = new Dbh();
  $rowsArr = $dbh->filterDesc("*", "device, commerce", "Storage", "Dname = Name");
}
else if (isset($_POST['fCamera'])) {
  $dbh = new Dbh();
  $rowsArr = $dbh->filterDesc("*", "device, commerce", "Camera", "Dname = Name");
}
else if (isset($_POST['fDisplay'])) {
  $dbh = new Dbh();
  $rowsArr = $dbh->filterDesc("*", "device, commerce", "Display", "Dname = Name");
}
else if (isset($_POST['fCarrier'])) {
  $dbh = new Dbh();
  $rowsArr = $dbh->getFilter("*", "device, commerce", "Carrier", "Dname = Name");
}

else {
  $dbh = new Dbh();
  $rowsArr = $dbh->getDefinedSelQuery("*", "device, commerce", "Dname = Name");
}

?>

<thead>
    <tr>
    <form method="post">   <!--Device-->
        <th><input type="submit" style="font-size: 18px; color: rgb(0, 0, 0); line-height: 1.2; font-weight: unset; 
            cursor: pointer; margin-left: 5px; background-color: #fff; border: none; padding: 3px;" 
            value="Device" name="fDevice"></th>
    </form>

    <form method="post">   <!--Year-->
        <th><input type="submit" style="font-size: 18px; color: rgb(0, 0, 0); line-height: 1.2; font-weight: unset; 
            cursor: pointer; margin-left: 5px; background-color: #fff; border: none; padding: 3px;" 
            value="Year" name="fYear"></th>
    </form>

    <form method="post">   <!--Company-->
        <th><input type="submit" style="font-size: 18px; color: rgb(0, 0, 0); line-height: 1.2; font-weight: unset; 
            cursor: pointer; margin-left: 5px; background-color: #fff; border: none; padding: 3px;" 
            value="Company" name="fCompany"></th>
    </form>
    
    <form method="post">   <!--RAM (in GB)-->
        <th><input type="submit" style="font-size: 18px; color: rgb(0, 0, 0); line-height: 1.2; font-weight: unset; 
            cursor: pointer; margin-left: 5px; background-color: #fff; border: none; padding: 3px;" 
            value="RAM (in GB)" name="fRAM"></th>
    </form>
    
    <form method="post">   <!--CPU-->
        <th><input type="submit" style="font-size: 18px; color: rgb(0, 0, 0); line-height: 1.2; font-weight: unset; 
            cursor: pointer; margin-left: 5px; background-color: #fff; border: none; padding: 3px;" 
            value="CPU" name="fCPU"></th>
    </form>
    
    <form method="post">   <!--OS-->
        <th><input type="submit" style="font-size: 18px; color: rgb(0, 0, 0); line-height: 1.2; font-weight: unset; 
            cursor: pointer; margin-left: 5px; background-color: #fff; border: none; padding: 3px;" 
            value="OS" name="fOS"></th>
    </form>
    
    <form method="post">   <!--IO-->
        <th><input type="submit" style="font-size: 18px; color: rgb(0, 0, 0); line-height: 1.2; font-weight: unset; 
            cursor: pointer; margin-left: 5px; background-color: #fff; border: none; padding: 3px;" 
            value="IO" name="fIO"></th>
    </form>
    
    <form method="post">  <!--Battery (in mAh-->
        <th><input type="submit" style="font-size: 18px; color: rgb(0, 0, 0); line-height: 1.2; font-weight: unset; 
            cursor: pointer; margin-left: 5px; background-color: #fff; border: none; padding: 3px;" 
            value="Battery (in mAh)" name="fBattery"></th>
    </form>
    
    <form method="post">   <!--Storage (in GB)-->
        <th><input type="submit" style="font-size: 18px; color: rgb(0, 0, 0); line-height: 1.2; font-weight: unset; 
            cursor: pointer; margin-left: 5px; background-color: #fff; border: none; padding: 3px;" 
            value="Storage (in GB)" name="fStorage"></th>
    </form>
    
    <form method="post">   <!--Camera-->
        <th><input type="submit" style="font-size: 18px; color: rgb(0, 0, 0); line-height: 1.2; font-weight: unset; 
            cursor: pointer; margin-left: 5px; background-color: #fff; border: none; padding: 3px;"
            value="Camera" name="fCamera"></th>
    </form>
    
    <form method="post">   <!--Display-->
        <th><input type="submit" style="font-size: 18px; color: rgb(0, 0, 0); line-height: 1.2; font-weight: unset; 
            cursor: pointer; margin-left: 5px; background-color: #fff; border: none; padding: 3px;"
            value="Display" name="fDisplay"></th>
    </form>
    
    <form method="post">   <!--Carrier-->
        <th><input type="submit" style="font-size: 18px; color: rgb(0, 0, 0); line-height: 1.2; font-weight: unset; 
            cursor: pointer; margin-left: 5px; background-color: #fff; border: none; padding: 3px;"
            value="Carrier" name="fCarrier"></th>
    </form>
</thead>
