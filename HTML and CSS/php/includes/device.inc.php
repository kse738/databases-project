<?php

include './../classes/dbh.class.php';
include './../classes/device.class.php';

// Checks if value entered first to allow two different html forms
// for the same file.
if(isset($_POST['dName'])) {
  $dName = (string) $_POST['dName'];
  $insbutton = $_POST['submitD'];

  // TODO Needs to accept NULL values
  if ($insbutton) {
    if (!empty($dName)) {
      $attr = array(
        "dName" => (string) $_POST["dName"],
        "year" => (int) $_POST["year"],
        "ram" => (int) $_POST["ram"],
        "cpu" => (string) $_POST["cpu"],
        "gpu" => (string) $_POST["gpu"],
        "os" => (string) $_POST["os"],
        "io" => (string) $_POST["io"],
        "battery" => (int) $_POST["battery"],
        "storage" => (int) $_POST["storage"],
        "camera" => (string) $_POST["camera"],
        "display" => (string) $_POST["display"],
        "deviceType" => (string) $_POST["dType"],
        "carrier" => (string) $_POST["carrier"],
        "keyboard" => (string) $_POST["keyboard"],
        "penComp" => (int) $_POST["penComp"]
      );

      $dev = new Device($attr["dName"],$attr["year"],$attr["deviceType"]);

      // If already exists in DB, update, otherwise, insert
      if ($dev->keyExists()) {
        foreach ($attr as $i => $value) {
          $dev->setAttribute($i, $value);
        }
        $dev->updateDevice();
      }
      else {
        foreach ($attr as $i => $value) {
          $dev->setAttribute($i, $value);
        }
      
        $dev->insDevice();
      }
    }
  }
}

if(isset($_POST['dNameDelete'])) {
  $dNameDel = (string) $_POST['dNameDelete'];
  $delbutton = $_POST['submitDdelete'];
  
  if ($delbutton) {
    if (!empty($dNameDel)) {
      $delDev = new Device($dNameDel,NULL,NULL);

      if ($delDev->keyExists()) {
        $delDev->deleteDevice($dNameDel);
      }
      else {
        echo "Device does not exist.";
      }
    }
  }
}



