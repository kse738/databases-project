<?php

include '../classes/dbh.class.php';
include '../classes/admin.class.php';

if (isset($_POST['login-submit'])) {


  $userName = $_POST['userName'];
  $password = $_POST['pwd'];

  $admin = new Admin($userName,$password);

  if (empty($userName) || empty($password)) {
    header("Location: ../adminLogin.php?error=emptyfields");
    exit();
  }
  else {
    $admin->adminLogin();
  }

}
else {
  header("Location: ../adminLogin.php");
  exit();
}