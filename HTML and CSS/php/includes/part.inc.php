<?php
//include 'class-autoload.inc.php';
include './../classes/dbh.class.php';
include './../classes/part.class.php';

$pName = (string) $_POST["pName"];
$manuf = (string) $_POST["manuf"];
$pType = (string) $_POST["pType"];


$part = new Part($pName,$manuf,$pType);

// If already exists in DB, update, otherwise, insert
if ($part->keyExists()) {
  $part->setManuf($manuf);
  $part->setpType($pType);

  $part->updatePart();
}
else {
  $part->insPart();
}

