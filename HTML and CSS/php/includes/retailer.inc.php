<?php

include './../classes/dbh.class.php';
include './../classes/retailer.class.php';

$rName = $_POST["rName"];
$web = $_POST["web"];

$ret = new Retailer($rName,$web);

if ($ret->keyExists() == true) {
  // If already exists in database
  $ret->setWeb($web);

  $ret->updateRetailer();
}
else {
  // Otherwise insert
  $ret->insRetailer();
}
