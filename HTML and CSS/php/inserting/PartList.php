<?php

include '../classes/dbh.class.php';
include '../classes/part.class.php';
include '../classes/retailer.class.php';

$part = new Part(NULL,NULL,NULL);
$pRowsArr = $part->getAllParts();
$ret = new Retailer(NULL,NULL);
$rRowsArr = $ret->getAllRetailers();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="description" content="Apple Brand Page">
    <title>
        Parts
    </title>
    <link href="../../css/apple.css" rel="stylesheet" type="text/css">
    <link href="../../css/input.css" rel="stylesheet" type="text/css">
</head>

<body>
  <!----------------------------PAGE HEADER AND NAIGATION BAR-------------------------->
  <?php
    require "../shopPageHeader.php";

    // Check to see if logged in as admin first
    if (!isset($_SESSION['userID'])) {
      header("Location: ../adminLogin.php?error=loginfirst");
      exit();
    }
  ?>
  <br/><br/><br/>
  <h2 style="font-size: 20pt; font-style: italic; color: rgb(61, 61, 61);">Parts and Retailers</h2>
  <br/><br/><br/><br/><br/>

  <!-------------------------------------------------INSERT PART------------------------------------------------------>
  <div class="in">
    <form class="form-inline" action="/php/includes/part.inc.php" method="post">
        <label style="margin-left: 15%;" for="Part name">Insert Part:</label>
        <input  type="text" name="pName" placeholder="Part name">
        <input type="text" name="manuf" placeholder="Manufacturer">
        <select style="border: 1px solid rgb(190, 190, 190); padding: 3px; color: rgb(133, 133, 133) "name="pType">
            <option value="Display">Display</option>
            <option value="RAM">RAM</option>
            <option value="CPU">CPU</option>
            <option value="GPU">GPU</option>
            <option value="OS">OS</option>
            <option value="IO">IO</option>
            <option value="Battery">Battery</option>
            <option value="Storage">Storage</option>
            <option value="Camera">Camera</option>
        </select>
        <button style="margin-left: 5px; border: 1px solid rgb(190, 190, 190); padding: 3px;"type="submit" name="submit">Insert</button>
    </form>
  
    <!-----------------------------------------------INSERT RETAILER---------------------------------------------------->
    <form style="float:right; margin-right:15%;"class="form-inline" action="/php/includes/retailer.inc.php" method="post">
        <label for="Retailer name" >Insert Retailer:</label>
        <input type="text" name="rName" placeholder="Retailer name">
        <input type="text" name="web" placeholder="Website">
        <button style="margin-left: 3px; border: 1px solid rgb(190, 190, 190); padding: 3px;" class="butt" type="submit" name="submit">Insert</button>
    </form>
  </div>
  <br/><br/><br/><br/>

<script>
    // For Search bars
    <?php
    require "../../js/searchBar.js";
    ?>
</script>

<div class="row">   <!---Allows tables to be shown side by side--->
    <div class="col">
        <!----------------------------Search Bar-------------------------->
        <form class="form-inline">
            <input type="text" style="margin-left: 45%; "id="pInput" onkeyup="searchTable('parts','pInput')" placeholder="Search Part Name">
            <br/><br/><br/><br/>
        </form>
        <!---------PART TABLE-------->
        <table class="table1" id="parts">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Manufacturer</th>
                    <th>Type</th>
                </tr>
            </thead>
            <tbody>
              <?php 
              foreach ($pRowsArr as $row) {
                echo "<tr>
                        <td>".$row['Name']."</td>
                        <td>".$row['Manufacturer']."</td>
                        <td>".$row['Type']."</td>
                     </tr>";
              }
              ?>
            </tbody>
        </table>
    </div>

    <div class="col">
        <!----------------------------Search Bar-------------------------->
        <form class="form-inline">
            <input type="text" style="margin-left:30%;" id="rInput" onkeyup="searchTable('retailers','rInput')" placeholder="Search Retailer Name">
            <br/><br/><br/><br/>
        </form>
        <table class="table2" id="retailers">
            <!------------RETAILER TABLE----------->
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Website</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                foreach ($rRowsArr as $row) {
                    echo "<tr>
                            <td>".$row['Name']."</td>
                            <td>".$row['Website']."</td>
                        </tr>";
              }
              ?>
            </tbody>
        </table>
    </div>
</div>

</body>
</html>