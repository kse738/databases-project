<?php
include '../classes/dbh.class.php';
include '../classes/device.class.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="description" content="Apple Brand Page">
    <title>
        Insert Devices
    </title>
    <link href="/css/apple.css" rel="stylesheet" type="text/css">
    <link href="/css/input.css" rel="stylesheet" type="text/css">
    <link href="/css/deviceInput.css" rel="stylesheet" type="text/css">
</head>

<body>
  <!----------------------------PAGE HEADER AND NAIGATION BAR-------------------------->
  <?php
    require "../shopPageHeader.php";

    // Check to see if logged in as admin first
    if (!isset($_SESSION['userID'])) {
      header("Location: ../adminLogin.php?error=loginfirst");
      exit();
    }

  ?>  
  <br/><br/><br/>
  <h2 style="font-size: 20pt; font-style: italic; color: rgb(61, 61, 61);">Edit Devices</h2>

  <!------------------------------ SEARCH BAR ----------------------------------->
  <script>
    <?php 
      require "../../js/searchBar.js";
    ?>
  </script>
  <form class="form-inline">
    <input type="text" style="margin-left: 78%; width: 10%;" id="dInput" onkeyup="searchTable('devices','dInput')" placeholder="Search Device Name">
  </form>
  <br/><br/><br/><br/><br/>
  
  <!-----------------------------------------------INSERT DEVICE---------------------------------------------------->
  <div class="in">
    <form class="form-inline" action="/php/includes/device.inc.php" method="post">
        <label style="margin-left: 11%;" for="Device name">Insert Device:</label>
        <input  type="text" name="dName" placeholder="Device Name">
        <input type="text" name="year" placeholder="Year">

        <input  style="width: 3%;" type="text" name="ram" placeholder="RAM">
        <input  style="width: 3%;" type="text" name="cpu" placeholder="CPU">
        <input  style="width: 3%;" type="text" name="gpu" placeholder="GPU">
        <input  style="width: 3%;" type="text" name="os" placeholder="OS">
        <input  style="width: 3%;" type="text" name="io" placeholder="IO">
        <input  type="text" name="battery" placeholder="Battery">
        <input  type="text" name="storage" placeholder="Storage">
        <input  type="text" name="camera" placeholder="Camera">
        <input  type="text" name="display" placeholder="Display">
        <input  type="text" name="carrier" placeholder="Carrier">
        <input  type="text" name="keyboard" placeholder="Keyboard">
        <input  type="text" name="penComp" placeholder="Pen Comp">

        <select style="border: 1px solid rgb(190, 190, 190); padding: 3px; color: rgb(133, 133, 133);" name="dType">
            <option value="phone">Smartphone</option>
            <option value="laptop">Laptop</option>
            <option value="tablet">Tablet</option>
        </select>


        <input style="cursor: pointer; margin-left: 5px; background-color: #ddd; border: 1px solid rgb(190, 190, 190); padding: 3px; width: 3%;" type="submit" name="submitD" value="Insert"/>
    </form>
    <br/><br/><br/><br/>

    <!-- To delete device -->
    <form class="form-inline" action="/php/includes/device.inc.php" method="post">
      <label style="margin-left: 43%;" for="Device name">Delete Device:</label>
        <input type="text" name="dNameDelete" placeholder="Device Name">
        <input style="cursor: pointer; margin-left: 5px; background-color: #ddd; border: 1px solid rgb(190, 190, 190); padding: 3px; width: 3%;" type="submit" name="submitDdelete" value="Delete"/>
    </form>
    <br/><br/><br/><br/><br/>

  <!----------------------------TABLE OF DEVICES-------------------------->
  <table class="smallerTable" id="devices">
      <?php
        require "../filters/filter.php";
      ?>
    <tbody>
      <?php 
        foreach ($rowsArr as $row) {
          echo "<tr>
                  <td>".$row['Name']."</td>
                  <td>".$row['Year']."</td>
                  <td>".$row['CName']."</td>
                  <td>".$row['RAM']."</td>
                  <td>".$row['CPU']."</td>
                  <td>".$row['OS']."</td>
                  <td>".$row['IO']."</td>
                  <td>".$row['Battery']."</td>
                  <td>".$row['Storage']."</td>
                  <td>".$row['Camera']."</td>
                  <td>".$row['Display']."</td>
                  <td>".$row['Carrier']."</td>
                </tr>";
        }
        
        
      ?>
    </tbody>
  </table>






</body>
</html>