<?php
    session_start();
?>

<?php
  if(isset($_POST['redirPart'])){
    header("Location: http://localhost/php/inserting/PartList.php");
    exit;
  }
else if(isset($_POST['redirDevice'])) {
    header("Location: http://localhost/php/inserting/insertDevices.php");
    exit;
  }
?>

<div class="navbar">
  <div class="subnav">
      <button class="subnavbtn">Shop Brands <i class="fa fa-caret-down"></i></button>
      <div class="subnav-content">
          <a href="company-pages/companyDevices.php?company=Apple" target="_self">Apple</a>
          <a href="company-pages/companyDevices.php?company=Dell" target="_self">Dell</a>
          <a href="company-pages/companyDevices.php?company=Google" target="_self">Google</a>
          <a href="company-pages/companyDevices.php?company=Microsoft" target="_self">Microsoft</a>
          <a href="company-pages/companyDevices.php?company=OnePlus" target="_self">OnePlus</a>
          <a href="company-pages/companyDevices.php?company=Samsung" target="_self">Samsung</a>
      </div>
  </div>
  <div class="subnav">
      <button class="subnavbtn">Shop Devices <i class="fa fa-caret-down"></i></button>
      <div class="subnav-content">
          <a href="allDevices.php" target="_self">All Devices</a>
          <a href="/php/deviceTypePage.php?type=laptop" target="_self">Laptops</a>
          <a href="/php/deviceTypePage.php?type=phone" target="_self">Smartphones</a>
          <a href="/php/deviceTypePage.php?type=tablet" target="_self">Tablets</a>
      </div>
  </div>
  <!--<div class="subnav">
      <button class="subnavbtn">Shop Trending <i class="fa fa-caret-down"></i></button>
      <div class="subnav-content">
          <a href="iPhone 11">iPhone 11</a>
          <a href="Samsung Galaxy Note10">Samsung Galaxy Note10</a>
          <a href="MacBook Pro 2019">MacBook Pro 2019</a>
          <a href="Mac Mini 2018">Mac Mini 2018</a>
          <a href="Microsoft Surface Go">Microsoft Surface Go</a>
      </div>
  </div>-->
  <!--< ?php
    if (isset($_SESSION['userID'])) {
        echo '<form class="subnav" action="/php/includes/logout.inc.php" method="post">
                <button type="submit" class="subnavbtn" name="logout-submit">Logout</button>
            </form>';
    }
  ?>-->
  <?php
    if (isset($_SESSION['userID'])) {
      echo 
           '<form method="post" class="subnav">
              <input type="submit" class="subnavbtn" style="cursor:pointer;" value="Insert Part or Retailer" name="redirPart">
              </input>
            </form>

            <form method="post" class="subnav">
              <input type="submit" class="subnavbtn" style="cursor:pointer;" value="Insert Device" name="redirDevice">
              </input>
            </form>

            <form class="subnav" action="/php/includes/logout.inc.php" method="post">
              <button type="submit" style="cursor:pointer;" class="subnavbtn" name="logout-submit">Logout</button>
            </form>';
    }
      else {
        echo
          '<form class="subnav" action="/php/includes/login.inc.php" method="post">
              <button type="submit"  style="cursor:pointer;" class="subnavbtn" name="login-submit">Admin Login</button>
          </form>';
      }
      
    ?>
  
</div>
<br/><br/><br/><br/><br/><br/><br/>