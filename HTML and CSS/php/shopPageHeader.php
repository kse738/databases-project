<?php
  session_start();
?>

<!----------------------------PAGE HEADER AND NAIGATION BAR-------------------------->
<br/>
<h1 class="shop-page">
    <img src="/css/EDB.png" style="height: 100px; width: 120px; margin-bottom: -40px;">
  <a href="/php/ED-menu.php" target="_self" style="text-decoration: none; color: rgb(41, 41, 41); margin-left: -10px;"><strong>ELECTRONICS DATABASE</strong></a>

</h1><br/>
<hr style="color: black; margin-left: 20px; margin-right: 20px;"/>
<br/><br/>

<div class="navbar">
  <div class="subnav">
    <a class="subnavbtn" href="/php/ED-menu.php" target="_self">Home </a>
  </div>
  <div class="subnav">
    <button class="subnavbtn">Shop Brands <i class="fa fa-caret-down"></i></button>
    <div class="subnav-content">
        <a href="/php/company-pages/companyDevices.php?company=Apple" target="_self">Apple</a>
        <a href="/php/company-pages/companyDevices.php?company=Dell" target="_self">Dell</a>
        <a href="/php/company-pages/companyDevices.php?company=Google" target="_self">Google</a>
        <a href="/php/company-pages/companyDevices.php?company=Microsoft" target="_self">Microsoft</a>
        <a href="/php/company-pages/companyDevices.php?company=OnePlus" target="_self">OnePlus</a>
        <a href="/php/company-pages/companyDevices.php?company=Samsung" target="_self">Samsung</a>
    </div>
  </div>
  <div class="subnav">
    <button class="subnavbtn">Shop Devices <i class="fa fa-caret-down"></i></button>
    <div class="subnav-content">
        <a href="/php/allDevices.php" target="_self">All Devices</a>
        <a href="/php/deviceTypePage.php?type=laptop" target="_self">Laptops</a>
        <a href="/php/deviceTypePage.php?type=phone" target="_self">Smartphones</a>
        <a href="/php/deviceTypePage.php?type=tablet" target="_self">Tablets</a>
    </div>
  </div>

  <?php
    if (isset($_SESSION['userID'])) {
      echo '<div class="subnav">
              <a class="subnavbtn" href="/php/inserting/PartList.php" target="_self">Insert Part or Retailer </a>
            </div>
            <div class="subnav">
              <a class="subnavbtn" href="/php/inserting/insertDevices.php" target="_self">Insert Devices </a>
            </div>

            <form class="subnav" action="/php/includes/logout.inc.php" method="post">
              <button type="submit" class="subnavbtn" name="logout-submit">Logout</button>
            </form>';
    }
    else {
      echo
        '<div class="subnav">
          <a class="subnavbtn" href="/php/adminLogin.php" target="_self" >Admin Login</a>
        </div>';
    }
  ?>
</div>
