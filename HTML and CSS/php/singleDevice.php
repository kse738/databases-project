<?php

include 'classes/dbh.class.php';
include 'classes/device.class.php';

$deviceName = "iPad Pro 11";  // Will need to code for onclick in table of devices

$dbh = new Dbh();
// Device
$dev = $dbh->getSingle("*", "device, commerce", "Name = DName and Name = '$deviceName'");

// Retailers where device is sold
$rRowsArr = $dbh->getDefinedSelQuery("*", "retailer, commerce", "Name = RName and DName = '$deviceName'");
?>




<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="description" content="Apple Brand Page">
    <title>
        Devices
    </title>
    <link href="/css/apple.css" rel="stylesheet" type="text/css">
    <link href="/css/devices.css" rel="stylesheet" type="text/css">
    <link href="/css/deviceInput.css" rel="stylesheet" type="text/css">
</head>

<body>
  <!----------------------------PAGE HEADER AND NAIGATION BAR-------------------------->
  <?php
    require "shopPageHeader.php";
  ?>
  <br/><br/><br/>
  <h2 style="font-size: 20pt; font-style: italic; color: rgb(61, 61, 61);"><?php echo $deviceName ?></h2>



  <br/><br/><br/><br/><br/>

  <!----------------------------SINGLE DEVICE-------------------------->
  <table id="devices">
    <thead>
      <tr>
        <th>Device Name</th>
        <th>Year</th>
        <th>Company</th>
        <th>RAM (in GB)</th>
        <th>CPU</th>
        <th>OS</th>
        <th>IO</th>
        <th>Battery (in mAh)</th>
        <th>Storage (in GB)</th>
        <th>Camera</th>
        <th>Display</th>
        <th>Carrier</th>
      </tr>
    </thead>
    <tbody>
      <?php 
      echo "<tr>
              <td>".$dev['Name']."</td>
              <td>".$dev['Year']."</td>
              <td>".$dev['CName']."</td>
              <td>".$dev['RAM']."GB</td>
              <td>".$dev['CPU']."</td>
              <td>".$dev['OS']."</td>
              <td>".$dev['IO']."</td>
              <td>".$dev['Battery']."mAh</td>
              <td>".$dev['Storage']."GB</td>
              <td>".$dev['Camera']."</td>
              <td>".$dev['Display']."</td>
              <td>".$dev['Carrier']."</td>
            </tr>";
      ?>
    </tbody>
  </table>

<!------------RETAILERS TABLE----------->
<table class="table2" id="retailers">
  <thead>
      <tr>
          <th>Name</th>
          <th>Price</th>
          <th>Website</th>
      </tr>
  </thead>
  <tbody>
      <?php 
      // Won't contain multiple retailers
      foreach ($rRowsArr as $row) {
          echo "<tr>
                  <td>".$row['Name']."</td>
                  <td>$".$row['Price']."</td>
                  <td>".$row['Website']."</td>
              </tr>";
    }
    ?>
  </tbody>
</table>

<br/><br/>
<?php
  require "footer.php";
?>


</body>
</html>
