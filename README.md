# Welcome to the Electronic Devices Database Project

### Created by Kevin Ehlen, Katherine Miller, and John Shenouda

#### Instaliation instructions

1. Download [XAMPP](https://www.apachefriends.org/download.html "XAMPP Download Page") to your computer, and be sure to choose the options to include Apache and MySQL in the installation. 
2. Open XAMPP and start Apache and MySQL
3. Run the script called `copyProject.bat`. This copies the contents of the `HTML and CSS` folder to the subfolder entitled `htdocs` (C:/xampp/htdocs)
4. Go to http://localhost/phpmyadmin 
5. Create a new database and name it `electronic-devices`
6. Go to `Import` (tab at the top), and upload the .sql file on your computer
7. If all steps were done correctly, the site should now be accessible via http://loacalhost/php/ED-menu.php

#### To import the database:

Make sure you are running `Apache` and `MySQL` in XAMPP.

Create a Database called `electronic-devices`

Go to http://localhost/phpmyadmin/server_import.php in a browser 
and choose `Electronic_Devices.sql`

#### To export the database:
 
Go to http://localhost/phpmyadmin/db_export.php?db=electronic_devices, hit `Go`
and just keep the same name `Electronic_Devices.sql`.